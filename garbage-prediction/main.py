import plot
import pandas as pd
from pandas.core.frame import DataFrame
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
import numpy as np

def read_csv(filename: str) -> DataFrame:
    """ Read data from a csv file and return a DataFrame """
    df: DataFrame = DataFrame(pd.read_csv(filename))
    df.head()
    return df


def scale_data(df: DataFrame):
    scaler = StandardScaler()
    final_df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)
    return final_df


def _pca(df: DataFrame, n_components: int = 2):
    pca = PCA(n_components=n_components)
    pca_result = pca.fit(df)
    return pca_result


def pca(df: DataFrame, n_components: int = 2) -> DataFrame:
    pca = PCA(n_components=n_components)
    pca_result = pca.fit_transform(df)
    return DataFrame(pca_result)


def principal_components(pca_model, initial_feature_names, pc_count: int = 2) -> list:
    most_important_pcs = [ np.abs(pca_model.components_[i].argmax()) for i in range(pc_count) ]
    most_important_names = [ initial_feature_names[most_important_pcs[i]] for i in range(pc_count) ]
    return most_important_names


def main():
    df = read_csv("./garbage_data/2019.csv")
    headers = df.columns.values
    regioni = df.pop('regione')

    scaled_df = scale_data(df)
    pca_df = pca(scaled_df)
    pca_model = _pca(scaled_df, 6)

    pcs = principal_components(pca_model, headers)
    print("Principal components are", " and ".join(pcs))

    pcs_dict = { pcs[i] : df[pcs[i]].tolist() for i in range(0,2) }
    plot.principal_components_plot(pcs_dict[pcs[0]], pcs_dict[pcs[1]], pcs[0], pcs[1], regioni)

    plot.component_importance_plot(pca_model)
    plot.correlation_heatmap_plot(headers, df)
    plot.scree_plot(pca_model)


if __name__ == "__main__":
    main()
