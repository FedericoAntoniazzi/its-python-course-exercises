import matplotlib.pyplot as plt
import numpy as np

plot_directory = "./plots"

def component_importance_plot(pca_result):
    _, ax = plt.subplots()
    variance = pca_result.explained_variance_ratio_
    variance = list(map(lambda v: v*100, variance))
    ax.bar(range(len(variance)), variance, width=0.4)
    plt.savefig(f"{plot_directory}/component_importance.png")


def correlation_heatmap_plot(cols, data):
    correlation = data.corr()
    fig = plt.figure(figsize=(20,20))
    ax = fig.add_subplot(111)
    cax = ax.matshow(correlation, vmin=-1, vmax=1)
    # Add colobar
    fig.colorbar(cax)
    ticks = np.arange(0,len(cols),1)
    # Add labels
    ax.set_xticks(ticks)
    ax.set_yticks(ticks)
    ax.set_xticklabels(cols)
    ax.set_yticklabels(cols)
    # Rotate labels to avoid "collisions"
    plt.xticks(rotation=20, ha="center")
    # Save graph
    plt.savefig(f"{plot_directory}/correlation_heatmap.png")


def scree_plot(pca_result):
    pc_values = np.arange(pca_result.n_components_) + 1
    _, ax = plt.subplots()
    ax.plot(pc_values, pca_result.explained_variance_ratio_, 'o-', linewidth=2, color="blue")
    ax.set_title('Scree plot')
    ax.set_xlabel("Principal component")
    ax.set_ylabel("Proportion of Variance Explained")
    plt.savefig(f"{plot_directory}/scree_plot.png")

def principal_components_plot(xdata, ydata, xlabel, ylabel, annotations):
    _, ax = plt.subplots()
    ax.scatter(xdata, ydata)
    ax.set_title("Principal components")

    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)

    for i, txt in enumerate(annotations):
        ax.annotate(txt, (xdata[i], ydata[i]))

    plt.savefig(f"{plot_directory}/principal_components_plot.png")
