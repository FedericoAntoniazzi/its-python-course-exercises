# Jumping frog

C'è una riga con 9 caselle, 4 rane verdi a sinistra e 4 rane rosse a destra, le rane verdi possono saltare solo verso destra mentre quelle rosse, solo verso sinistra.
Ciascuna rana può muoversi nella casella successiva MA qualora vi fosse una rana di colore diverso, può saltarla e andare nella casella successiva.

Risolvere il gioco in python
