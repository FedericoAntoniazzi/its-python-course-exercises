frogs_count = 3
field = "G" * frogs_count + " " + "R" * frogs_count

red_moves = {
    " R": "R ",
    " GR": "RG "
}
green_moves = {
    "G ": " G",
    "GR ": " RG"
}

def move(moves:dict):
    global field
    for pattern, replace in moves.items():
        if pattern in field:
            field = field.replace(pattern, replace, 1)
            break

i = 0
def move_loop(moves: dict, count: int):
    global i
    for _ in range(0, count):
        move(moves)
        # print(field)
        i+=1
        print("Move",str(i).zfill(2),":",field)

def main():
    print("Begin:",field)

    for count in range(1, frogs_count+1):
        if count % 2:
            move_loop(green_moves, count)
        else:
            move_loop(red_moves, count)

    if frogs_count % 2:
        move_loop(red_moves, frogs_count)
    else:
        move_loop(green_moves, frogs_count)

    for count in reversed(range(1, frogs_count+1)):
        if count % 2:
            move_loop(green_moves, count)
        else:
            move_loop(red_moves, count)

if __name__ == '__main__':
    main()
