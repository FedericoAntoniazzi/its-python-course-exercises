token_count = 2
token_a = "A"
token_b = "B"

initial_state = token_a * token_count + " " + token_b * token_count
final_state = token_b * token_count + " " + token_a * token_count

moves = {
    " B": "B ",
    " AB": "BA ",
    "A ": " A",
    "AB ": " BA"
}

def generate_next_state(current_state: str) -> list:
    next_states = list()

    for pattern, replacement in moves.items():
        # If pattern is in the current state
        if current_state.find(pattern) != -1:
            new_state = current_state.replace(pattern, replacement)
            next_states.append(new_state)

    return next_states

def main():
    states_queue = [[initial_state]]

    while len(states_queue) > 0:
        path = states_queue.pop()
        current_state = path[-1]

        if current_state == final_state:
            print(path)
        else:
            next_possibile_states = generate_next_state(current_state)
            for state in next_possibile_states:
                new_path = list(path)
                new_path.append(state)
                states_queue.append(new_path)

if __name__ == '__main__':
    main()
