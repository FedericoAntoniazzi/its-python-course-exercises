import itertools

names = [
    "george", "mum",
    "spencer", "kydd", "elizabeth", "philip", "margaret",
    'diana', 'charles', 'anne', 'mark', 'andrew', 'sarah', 'edward', 'sophie',
    'william', 'harry', 'peter', 'zara', 'beatrice', 'eugenie', 'louise', 'james'
]

father_positive_examples = [
    ('george', 'elizabeth'), ('george', 'margaret'),
    ('spencer', 'diana'),
    ('philip', 'charles'), ('philip', 'anne'), ('philip', 'andrew'), ('philip', 'edward'),
    ('charles', 'william'), ('charles', 'harry'),
    ('mark', 'peter'), ('mark', 'zara'),
    ('andrew', 'beatrice'), ('andrew', 'eugenie'),
    ('edward', 'louise'), ('edward', 'james'),
]

mother_positive_examples = [
    ('mum', 'elizabeth'), ('mum', 'margaret'),
    ('kydd', 'diana'),
    ('elizabeth', 'charles'), ('elizabeth', 'anne'), ('elizabeth', 'andrew'), ('elizabeth', 'edward'),
    ('diana', 'william'), ('diana', 'harry'),
    ('anne', 'peter'), ('anne', 'zara'),
    ('sarah', 'beatrice'), ('sarah', 'eugenie'),
    ('sophie', 'louise'), ('sophie', 'james'),
]

parent_positive_examples = father_positive_examples + mother_positive_examples

grandfathers_positive_examples = [
    ('george', 'charles'),
    ('george', 'anne'),
    ('george', 'andrew'),
    ('george', 'edward'),
    ('philip', 'william'),
    ('philip', 'harry'),
    ('philip', 'peter'),
    ('philip', 'zara'),
    ('philip', 'beatrice'),
    ('philip', 'eugenie'),
    ('philip', 'louise'),
    ('philip', 'james'),
    ('spencer', 'william'),
    ('spencer', 'harry')
]

grandmothers_positive_examples = [
    ('mum', 'charles'),
    ('mum', 'anne'),
    ('mum', 'andrew'),
    ('mum', 'edward'),
    ('elizabeth', 'william'),
    ('elizabeth', 'harry'),
    ('elizabeth', 'peter'),
    ('elizabeth', 'zara'),
    ('elizabeth', 'beatrice'),
    ('elizabeth', 'eugenie'),
    ('elizabeth', 'louise'),
    ('elizabeth', 'james'),
    ('kydd', 'william'),
    ('kydd', 'harry')
]

grandparents_positive_examples = grandfathers_positive_examples + grandmothers_positive_examples


def father(x, y):
    return (x, y) in father_positive_examples


def mother(x, y):
    return (x, y) in mother_positive_examples


def parent(x, y):
    return father(x, y) or mother(x, y)


def foil_grandfathers():
    positive_grandfathers = []

    for x, y, z in itertools.combinations(names, 3):
        if (father(x, y) and father(y, z)) or (father(x, y) and mother(y, z)):
            positive_grandfathers.append((x,z))

    return positive_grandfathers


def foil_grandfathers_2():
    positive_grandfathers = []

    for x, y, z in itertools.combinations(names, 3):
        if (father(x, y) and parent(y, z)):
            positive_grandfathers.append((x,z))

    return positive_grandfathers


def foil_grandparents():
    positive_grandparents = []

    for x, y, z in itertools.combinations(names, 3):
        if parent(x, y) and parent(y, z):
            positive_grandparents.append((x, z))

    return positive_grandparents


# Accuracy < 1.0 -> FOIL has learned less samples than the real ones
# Accuracy = 1.0 -> All learned samples are correct
# Accuracy > 1.0 -> FOIL has learned more samples than the real ones
def producer_accuracy(produced_samples, total_samples):
    diffset = set(total_samples) - set(produced_samples)
    correct = set(produced_samples) - set(diffset)
    print("Producer accuracy:", len(correct)/len(total_samples))


def main():
    print("-- Grandfathers --")
    grandfathers = foil_grandfathers()
    producer_accuracy(grandfathers, grandfathers_positive_examples)

    print("-- Grandfathers 2 --")
    grandfathers_2 = foil_grandfathers_2()
    producer_accuracy(grandfathers_2, grandfathers_positive_examples)

    print("-- Grandparents --")
    grandparents = foil_grandparents()
    producer_accuracy(grandparents, grandparents_positive_examples)

if __name__ == "__main__":
    main()
