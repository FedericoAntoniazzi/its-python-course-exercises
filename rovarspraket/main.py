def decode(message: str) -> str:
    # Save decoded message in a list because strings are immutable
    decoded_message = []

    i = 0
    # if character is vowel => keep it and go to next character
    # else character is a consonant so the next 2 caracters must be ignored
    while i < len(message):
        character = message[i]
        decoded_message.append(character)
        
        if character.isalpha() and character in 'aeiou':
            i += 1
        else:
            i += 3

    return "".join(decoded_message)


def encode(message: str) -> str:
    # Save encoded message in a list because strings are immutable
    encoded_message = []
    vowels = "aeiou"

    for char in message:
        if char.isalpha() and char in vowels:
            encoded_message.append(char)
        else:
            encoded_message.extend([char, "o", char])

    return "".join(encoded_message)


def main():
    assert encode("mangiare") == "momanongogiarore"
    assert decode("momanongogiarore") == "mangiare"
    assert decode("cocibobo") == "cibo"
    assert decode("bobanonanona") == "banana"

if __name__ == "__main__":
    main()
