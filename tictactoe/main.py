import secrets

from itertools import combinations
from typing import List


class Player:
    """
    Class to represent a player

    Attributes
    ----------
    token : str
        Symbol used by the player to play (e.g cross, circle)

    Methods
    -------
    play():
        Player makes a random choice.
    """
    def __init__(self, token: str):
        self.token = token
        self.choices = []

    def play(self, illegal_choices: List) -> None:
        """
        Player makes a random choice
        """
        while True:
            choice = secrets.randbelow(9) + 1
            if (choice not in self.choices) and (choice not in illegal_choices):
                self.choices.append(choice)
                return

    def has_won(self) -> bool:
        """
        Check if player did a winning move.
        Uses the method of the magic square so it checks if the sum of one combination of its moves is equal to 15
        """
        won = False
        if len(self.choices) >= 3:
            combs = combinations(self.choices, 3)
            for comb in combs:
                if sum(comb) == 15:
                    won = True

        return won


def pretty_print(player1: Player, player2: Player) -> None:
    """ Prints the game board """
    magic_square = [
        [2, 7, 6],
        [9, 5, 1],
        [4, 3, 8]
    ]

    placeholder = "-"
    output_board: List[List[str]] = []
    for _ in range(3):
        output_board.append([placeholder] * 3)

    for i in range(3):
        for j in range(3):
            if magic_square[i][j] in player1.choices:
                output_board[i][j] = player1.token
            elif magic_square[i][j] in player2.choices:
                output_board[i][j] = player2.token

    for token in output_board:
        print(" ".join(token))


def main():
    player1, player2 = Player("x"), Player("o")

    for i in range(9):
        if not i % 2:
            player1.play(player2.choices)
        else:
            player2.play(player1.choices)

        if player1.has_won():
            print(f"Player 1 ({player1.token}) won!")
            break
        elif player2.has_won():
            print(f"Player 2 ({player2.token}) won!")
            break

    pretty_print(player1, player2)


if __name__ == '__main__':
    main()
