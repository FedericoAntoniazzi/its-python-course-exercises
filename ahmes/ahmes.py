# 1650 a.C papiro di Ahmes
# Gli egizi sanno fare solo le operazioni /2 e *2 e tutte le somme
# 
# Algoritmo di Ahmes
# 1) se a=1 allora a*b = b
# 2) se a è pari allora a*b = a/2 * 2b
# 3) se a è dispari e maggiore di 1 allora a*b = (a-1)*b + b
#
# Esempio
# a=5 b=7    5*7 = (5-1)*7 + 7 = 4*7 + 7 = 4/2 * 2 * 7 + 7 = 2 * 14 + 7 = 2/2 * 2*14 + 7 = 28 + 7 = 35

def is_even(n: int) -> bool:
    return not bool(n % 2)


def ahmes(a: int, b: int) -> int:
    result = 0

    # 1) se a=1 allora a*b = b
    if a == 1:
        result = b

    # 2) se a è pari allora a*b = a/2 * 2b
    elif is_even(a):
        result = (a // 2) * (2 * b)

    # 3) se a è dispari e maggiore di 1 allora a*b = (a-1)*b + b
    elif (not is_even(a)) and (a >= 1):
        result = ahmes(a - 1, b) + b

    return result


def test(a: int, b: int):
    # print(f"{a} * {b} | ahmes={ahmes(a,b)} | expected={a*b}")
    assert ahmes(a, b) == (a * b)


def main():
    # 1) se a=1 allora a*b = b
    test(1, 2)
    test(1, 3)
    test(1, 4)

    # 2) se a è pari allora a*b = a/2 * 2b
    test(2, 2)
    test(2, 3)
    test(2, 4)
    test(4, 7)
    test(8, 8)
    test(20, 9)

    # 3) se a è dispari e maggiore di 1 allora a*b = (a-1)*b + b
    test(5, 7)
    test(7, 5)
    test(13, 19)

if __name__ == "__main__":
    main()
