def is_square(matrix: list) -> bool:
    rows = len(matrix)

    for row in matrix:
        column_length = len(row)
        if column_length != rows:
            return False

    return True

def is_identity(matrix: list) -> bool:
    if not is_square(matrix):
        raise ArithmeticError("Unable to check if it is a identity matrix because it's not square")

    size = len(matrix)
    for row in range(0, size):
        for col in range(0, size):
            if col == row:
                if not matrix[row][col] == 1:
                    return False
            else:
                if not matrix[row][col] == 0:
                    return False

    return True

def generate_identity_matrix(size: int) -> list:
    matrix = []
    
    for x in range(0, size):
        row = [0] * size
        row[x] = 1
        matrix.append(row)

    return matrix

def _determinant_2x2(matrix: list):
    a, b, c, d = matrix[0][0], matrix[0][1], matrix[1][0], matrix[1][1]
    return a*d - b*c

def calculate_determinant(matrix: list):
    if not is_square(matrix):
        raise ArithmeticError("Unable to check if it is a identity matrix because it's not square")
    
    if len(matrix) == 2:
        return _determinant_2x2(matrix)

    raise NotImplementedError("Matrix size not yet unsupported")

def _inverse_2x2(matrix: list) -> list:
    a, b, c, d = matrix[0][0], matrix[0][1], matrix[1][0], matrix[1][1]
    return [
        [ d, -b ],
        [ -c, a ]
    ]

def calculate_inverse_matrix(matrix: list):
    if not is_square(matrix):
        raise ArithmeticError("Error while calculating inverse matrix: matrix is not square")

    if calculate_determinant(matrix) == 0:
        raise ArithmeticError("Error while calculating inverse matrix: Determinant is equal to 0 so the matrix cannot be inverted")
    
    if len(matrix) == 2:
        return _inverse_2x2(matrix)

    raise NotImplementedError("Matrix size not yet unsupported")

def main():
    matrix = [
        [ 1, 4 ],
        [ 3, 2 ]
    ]
    print("Matrix:", matrix)
    print("Square:", is_square(matrix))
    try:
        print("Identity:", is_identity(matrix))
    except ArithmeticError:
        print("Cannot check if matrix is identity: it's not square")

    try:
        print("Inverse:", calculate_inverse_matrix(matrix))
    except ArithmeticError:
        print("Cannot cannot calculate inverse matrix: it's not square")

    try:
        print("Determinant: ", calculate_determinant(matrix))
    except ArithmeticError:
        print("Cannot cannot calculate the determinant of the matrix: it's not square")
    

if __name__ == "__main__":
    main()
